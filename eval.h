#ifndef EVAL_H
#define EVAL_H

#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "stack.h"

typedef struct DictEntry {
	bool interpret;
	bool compile;
	bool primitive;
	size_t len;
	CorthToken* word;
} DictEntry;

DictEntry* init_dict_entry(size_t len, void* word, bool interp, bool comp, bool prim);
HashTable* init_default_dictionary();
size_t tokenize(size_t len, char* code, char*** token_buf);
size_t parse(size_t len, char** tokens, CorthToken** ast);
void interpret(size_t len, CorthToken* ast, CorthStack* ds, CorthStack* rs, HashTable* dict, int* ast_pos, bool* compile);
void compile(size_t len, CorthToken* ast, CorthStack* ds, CorthStack* rs, HashTable* dict, int* ast_pos, bool* compile);
void evaluate(size_t len, CorthToken* ast, CorthStack* ds, CorthStack* rs, HashTable* dict);

#endif // EVAL_H
