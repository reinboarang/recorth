#ifndef STACK_H
#define STACK_H

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct CorthStack {
	size_t size;
	int pos;
	int* stack;
} CorthStack;

CorthStack* init_stack(size_t size);

void push_stack(CorthStack* stack, int value);
int pop_stack(CorthStack* stack);

void place_stack(CorthStack* stack, int value);
int peek_stack(CorthStack* stack);

void print_stack(CorthStack* stack);

#endif // STACK_H
