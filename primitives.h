#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <stdbool.h>
#include <string.h>
#include "stack.h"
#include "hashtable.h"
#include "token.h"
#include "eval.h"

typedef void (*PrimitiveFunctionPtr)(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);

// Basic arithmetic
void corth_add(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_sub(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_mul(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_div(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);

// Basic stack operations

void corth_swap(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_dup(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_over(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_drop(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_rot(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_rev_rot(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_nip(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_tuck(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_pick(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_roll(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);

// Return stack operations

void corth_from_r(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_to_r(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_peek_r(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);

// Word definition operations

void corth_colon(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_semicolon(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_immediate(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_compile_only(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);


// Comment words

void corth_left_paren(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);
void corth_right_paren(CorthStack*, CorthStack*, HashTable*, bool*, CorthToken*, int*);

#endif // PRIMITIVES_H
