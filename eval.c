#include "hashtable.h"
#include "token.h"
#include "primitives.h"
#include "eval.h"

DictEntry* init_dict_entry(size_t len, void* word, bool interp, bool comp, bool prim) {
	DictEntry* entry = malloc(sizeof(DictEntry));
	entry->len = len;
	entry->word = word;
	entry->interpret = interp;
	entry->compile = comp;
	entry->primitive = prim;
	return entry;
}

void print_dict_entry(DictEntry* entry) {
	printf("|=== Word Definition ===|\n");
	printf("Word Length: %d\n", entry->len);
	printf("Interpret?: %s\n", entry->interpret ? "true" : "false");
	printf("Compile?: %s\n", entry->compile ? "true" : "false");
	printf("Primitive?: %s\n", entry->primitive ? "true" : "false");
	for ( int i = 0 ; i < entry->len ; i++ ) {
		print_token(entry->word[i]);
	}
	printf("|=======================|\n");
}


// taken from somewhere on StackOverflow, not my creation
size_t strlcpy(char* dst, const char* src, size_t bufsize) // memory safe strcpy
{
	size_t srclen = strlen(src);
	size_t result = srclen; /* Result is always the length of the src string */
	if(bufsize>0)
	{
		if(srclen>=bufsize)
			srclen=bufsize-1;
		if(srclen>0)
			memcpy(dst,src,srclen);
		dst[srclen]='\0';
	}
	return result;
}

size_t tokenize(size_t len, char* code, char*** token_buf) {
	int cur_pos = 0;
	size_t cur_len = 0;
	*token_buf = malloc(sizeof(char*));
	size_t result_ind = 0;
	while  ( cur_pos < len ) {
		cur_len = strcspn(&code[cur_pos], " \n\t");
		if ( cur_len < 1 ) {
			cur_pos++;
			continue;
		}
		*token_buf = realloc(*token_buf, sizeof(char*) * (result_ind + 1));
		(*token_buf)[result_ind] = malloc(sizeof(char) * cur_len + 1);
		strlcpy((*token_buf)[result_ind], &code[cur_pos], cur_len + 1);
		result_ind++;
		cur_pos += cur_len + 1;
	}
	return result_ind;
}

size_t parse(size_t len, char** tokens, CorthToken** ast) {
	*ast = malloc(sizeof(CorthToken) * len);
	for ( int i = 0 ; i < len ; i++ ) {
		size_t cur_len = strlen(tokens[i]);
		if ( strspn(tokens[i], "0123456789") == cur_len ) {
			(*ast)[i].type = NUMBER;
			(*ast)[i].value.number = atoi(tokens[i]);
		} else {
			(*ast)[i].type = IDENTIFIER;
			(*ast)[i].value.string = strdup(tokens[i]);
		}
	}
	return len;
}

#define ADD_DEFAULT_WORD(hash, name, ptr, interp, compile) {\
		set_hash(hash, init_entry(name, init_dict_entry(0, ptr, interp, compile, true))); \
}

HashTable* init_default_dictionary() {
	HashTable* default_dict = init_hash();

	ADD_DEFAULT_WORD(default_dict, "+", corth_add, true, false);
	ADD_DEFAULT_WORD(default_dict, "-", corth_sub, true, false);
	ADD_DEFAULT_WORD(default_dict, "*", corth_mul, true, false);
	ADD_DEFAULT_WORD(default_dict, "/", corth_div, true, false);


	ADD_DEFAULT_WORD(default_dict, "swap", corth_swap, true, false);
	ADD_DEFAULT_WORD(default_dict, "dup", corth_dup, true, false);
	ADD_DEFAULT_WORD(default_dict, "over", corth_over, true, false);
	ADD_DEFAULT_WORD(default_dict, "drop", corth_drop, true, false);
	ADD_DEFAULT_WORD(default_dict, "rot", corth_rot, true, false);
	ADD_DEFAULT_WORD(default_dict, "-rot", corth_rev_rot, true, false);
	ADD_DEFAULT_WORD(default_dict, "nip", corth_nip, true, false);
	ADD_DEFAULT_WORD(default_dict, "tuck", corth_tuck, true, false);
	ADD_DEFAULT_WORD(default_dict, "pick", corth_pick, true, false);

	ADD_DEFAULT_WORD(default_dict, "r>", corth_from_r, true, false);
	ADD_DEFAULT_WORD(default_dict, ">r", corth_to_r, true, false);
	ADD_DEFAULT_WORD(default_dict, "r@", corth_peek_r, true, false);

	ADD_DEFAULT_WORD(default_dict, ":", corth_colon, true, false);
	ADD_DEFAULT_WORD(default_dict, ";", corth_semicolon, false, true);
	ADD_DEFAULT_WORD(default_dict, "immediate", corth_immediate, true, true);
	ADD_DEFAULT_WORD(default_dict, "compile-only", corth_compile_only, true, true);

	ADD_DEFAULT_WORD(default_dict, "(", corth_left_paren, true, true);
	ADD_DEFAULT_WORD(default_dict, ")", corth_right_paren, true, true);

	return default_dict;
}


//void compile(size_t len, CorthToken* ast, CorthStack* ds, CorthStack* rs, HashTable* dict, int* ast_pos, bool* compile) {
//	while ( *ast_pos < len ) {
//		
//	}
//}

void interpret(size_t len, CorthToken* ast, CorthStack* ds, CorthStack* rs, HashTable* dict, int* ast_pos, bool* compile) {
	while ( *ast_pos < len ) {
		CorthToken cur_tok = ast[*ast_pos];
		printf("compile: %s\n", *compile ? "true" : "false");
		print_token(cur_tok);
		if ( cur_tok.type == NUMBER ) {
			if ( *compile ) { 
				printf("compile into current definition\n");
				DictEntry* cur_entry = dict->last_entry->value;
				cur_entry->len++;
				cur_entry->word = realloc(cur_entry->word, sizeof(CorthToken) * cur_entry->len);
				cur_entry->word[cur_entry->len-1] = ast[*ast_pos];
			} else {
				printf("push to data stack\n");
				push_stack(ds, cur_tok.value.number);
			}
		} else if ( cur_tok.type == IDENTIFIER ) {
			DictEntry* dict_entry = get_hash_value(dict, cur_tok.value.string);

			// current word not found
			if ( dict_entry == NULL ) { 
				printf("Word '%s' not found.\n", cur_tok.value.string);
				*ast_pos = len;
				return;

			// word found and is executable in current mode
			} else if ( (dict_entry->interpret && !(*compile)) || (dict_entry->compile && *compile) ) {
				printf("word found in dictionary\n");

				// current word is a predefined primitive
				if ( dict_entry->primitive ) {
					printf("executing word as primitive\n");
					PrimitiveFunctionPtr fun_ptr = (void*)dict_entry->word;
					(*fun_ptr)(ds,rs,dict,compile,ast,ast_pos);

				// current word is user defined
				} else {
					printf("executing word as user-defined\n");
					print_dict_entry(dict_entry);
					int new_pos = 0;
					bool new_compile = false;
					printf("\n");
					interpret(dict_entry->len, dict_entry->word, ds, rs, dict, &new_pos, &new_compile);
				}

			// in compile mode and token is not executable in current mode
			} else if ( *compile ) { 
				printf("compiling into current definition\n");
				DictEntry* cur_entry = dict->last_entry->value;
				cur_entry->len++;
				cur_entry->word = realloc(cur_entry->word, sizeof(CorthToken) * cur_entry->len);
				cur_entry->word[cur_entry->len-1] = ast[*ast_pos];
			}

		}
		// advance to next token
		(*ast_pos)++;
		printf("\n");
	}
}

void evaluate(size_t len, CorthToken* ast, CorthStack* ds, CorthStack* rs, HashTable* dict) {
	bool compile_mode = false;
	int ast_pos = 0;
	while ( ast_pos < len ) {
		if ( compile_mode ) {
			
		} else {
			interpret(len, ast, ds, rs, dict, &ast_pos, &compile_mode);
		}
	}
}

