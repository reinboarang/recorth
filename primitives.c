#include "primitives.h"

void corth_add(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, a + b);
}

void corth_sub(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, b - a);
}

void corth_mul(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, a * b);
}

void corth_div(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, b / a);
}

void corth_swap(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, a);
	push_stack(ds, b);
}

void corth_dup(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	push_stack(ds, peek_stack(ds));
}

void corth_over(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, b);
	push_stack(ds, a);
	push_stack(ds, b);
}

void corth_drop(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	pop_stack(ds);
}

void corth_rot(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	int c = pop_stack(ds);
	push_stack(ds, b);
	push_stack(ds, a);
	push_stack(ds, c);
}

void corth_rev_rot(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	int c = pop_stack(ds);
	push_stack(ds, a);
	push_stack(ds, c);
	push_stack(ds, b);
}

void corth_nip(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, a);
}

void corth_tuck(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int a = pop_stack(ds);
	int b = pop_stack(ds);
	push_stack(ds, b);
	push_stack(ds, a);
	push_stack(ds, b);
}

void corth_pick(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int n = pop_stack(ds);
	if ( (ds->pos - n) > -1 )
		push_stack(ds, ds->stack[ds->pos - n]);
}

void corth_from_r(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	push_stack(ds, pop_stack(rs));
}

void corth_to_r(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	push_stack(rs, pop_stack(ds));
}

void corth_peek_r(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	push_stack(ds, peek_stack(rs));
}

void corth_colon(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	*compile = true;
	char* new_word_name = ast[++(*ast_pos)].value.string;
	HashTableEntry* new_word = init_entry(new_word_name, init_dict_entry(0, NULL, true, false, false));
	set_hash(dict, new_word);
}

void corth_semicolon(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	*compile = false;
}

void corth_immediate(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	DictEntry* cur_entry = dict->last_entry->value;
	cur_entry->compile = true;
}

void corth_compile_only(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	DictEntry* cur_entry = dict->last_entry->value;
	cur_entry->compile = true;
	cur_entry->interpret = false;
}

void corth_left_paren(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	int i = *ast_pos;
	// scan for matching ')' token and advance the IP to that token
	while ( ast[i].type == NUMBER || (ast[i].type == IDENTIFIER && strcmp(ast[i].value.string, ")") != 0) ) {
		i++;
	}
	*ast_pos = i;
}

void corth_right_paren(CorthStack* ds, CorthStack* rs, HashTable* dict, bool* compile, CorthToken* ast, int* ast_pos) {
	// no need to do anything
}

