#ifndef TOKEN_H
#define TOKEN_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>

typedef enum CorthTokenType {
	NUMBER,
	IDENTIFIER,
	ERROR
} CorthTokenType;

typedef struct CorthToken {
	CorthTokenType type;
	union {
		char* string;
		int number;
	} value;
} CorthToken;

CorthToken init_token(CorthTokenType type, void* value);

void print_token(CorthToken token);
void print_tokens(size_t len, CorthToken* tokens);

#endif // TOKEN_H
