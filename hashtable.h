#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define HASH_KEYSPACE 1000

typedef struct HashTableEntry {
	char* key;
	void* value;
	struct HashTableEntry* next;
} HashTableEntry;

typedef struct HashTable {
	HashTableEntry* last_entry;
	HashTableEntry** table;
	int size;
} HashTable;

uint32_t generate_hash(char* src);

HashTableEntry* init_entry(char* key, void* value);

HashTable* init_hash();

void delete_hash(HashTable* table);

HashTable* set_hash(HashTable* table, HashTableEntry* entry);

HashTable* replace_hash(HashTable* table, char* dst, char* src);

HashTableEntry* get_hash(HashTable* table, char* key);

void* get_hash_value(HashTable* table, char* key);

#endif // HASHTABLE_H

