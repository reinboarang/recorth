#include "stack.h"

CorthStack* init_stack(size_t size) {
	CorthStack* new_stack = malloc(sizeof(CorthStack));
	new_stack->size = size;
	new_stack->pos = -1;
	new_stack->stack = malloc(sizeof(int) * size);
	for ( int i = 0 ; i < new_stack->size ; i++ ) {
		new_stack->stack[i] = 0;
	}
	return new_stack;
}

void push_stack(CorthStack* stack, int value) {
	stack->pos++;
	if ( stack->pos >= stack->size ) {
		stack->size++;
		stack->stack = realloc(stack->stack, sizeof(uint) * stack->size);
	}
	stack->stack[stack->pos] = value;
}

int pop_stack(CorthStack* stack) {
	int pop_value = stack->stack[stack->pos];
	if ( stack->pos > -1 ) {
		stack->pos--;
	} else {
		return -1;
	}
	return pop_value;
}

void place_stack(CorthStack* stack, int value) {
	stack->stack[stack->pos] = value;
}

int peek_stack(CorthStack* stack) {
	return stack->stack[stack->pos];
}

void print_stack(CorthStack* stack) {
	for ( int i = 0 ; i < stack->size ; i++ ) {
		printf("%d", stack->stack[i]);
		if ( i == stack->pos ) {
			printf("\t<==== %d", i);
		} else if ( i == 0 || i == (stack->size - 1) ) {
			printf("\t<-- %d", i);
		}
		printf("\n");
	}
}
