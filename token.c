#include "token.h"

CorthToken init_token(CorthTokenType type, void* value) {
	CorthToken tok;
	tok.type = type;
	tok.value.number = value;
	return tok;
}

void print_token(CorthToken token) {
	if ( token.type == NUMBER ) {
		printf("type: NUMBER value: %d\n", token.value.number);
	} else if ( token.type == IDENTIFIER ) {
		printf("type: IDENTIFIER value: '%s'\n", token.value.string);
	}
}

void print_tokens(size_t len, CorthToken* tokens) {
	for ( int i = 0 ; i < len ; i++ ) {
		print_token(tokens[i]);
	}
}
