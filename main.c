#include <stdio.h>
#include <stdlib.h>
#include "token.h"
#include "stack.h"
#include "hashtable.h"
#include "eval.h"

int main(int argc, char** argv) {

	if ( argc < 2 ) {
		printf("Code is required.\n");
		return -1;
	}

	size_t code_len = strlen(argv[1]);
	char** tokens = NULL;
	size_t tokens_len = tokenize(code_len, argv[1], &tokens);
	CorthToken* parsed_tokens = NULL;
	parse(tokens_len, tokens, &parsed_tokens);
	CorthStack* data_stack = init_stack(10);
	CorthStack* return_stack = init_stack(10);
	HashTable* dictionary = init_default_dictionary();
	evaluate(tokens_len, parsed_tokens, data_stack, return_stack, dictionary);
	print_stack(data_stack);

	return 0;
}
