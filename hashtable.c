#include "hashtable.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void print_hash(HashTable* ht) {
	printf("SIZE: %d\n", ht->size);
	for ( int i = 0 ; i < ht->size ; i++ ) {
		if ( ht->table[i] ) {
			HashTableEntry* cur_entry = ht->table[i];
			while ( cur_entry ) {
				printf("i: %d addr: 0x%x key: %s\n", i, cur_entry, cur_entry->key);
				//printf("key: %s\tvalue: %s\thash: %d\n", cur_entry->key, cur_entry->value, generate_hash(cur_entry->key));
				cur_entry = cur_entry->next;
			}
		}
	}
}


uint32_t generate_hash(char* src) {
	if ( src == NULL )
		return 0;
	uint32_t result = 0;
	for ( int i = 0 ; src[i] != '\0' ; i++ ) {
		result = result + src[i];
		if ( i > 0 ) {
			result = result ^ src[i-1];
		}
		result = result % HASH_KEYSPACE;
	}
	return result;
}

HashTableEntry* init_entry(char* key, void* value) {
	HashTableEntry* he = malloc(sizeof(HashTableEntry));
	he->key = key;
	he->value = value;
	he->next = NULL;
	return he;
}

HashTable* init_hash() {
	HashTable* tmp = malloc(sizeof(HashTable));
	tmp->table = malloc(sizeof(HashTableEntry*));
	tmp->table[0] = init_entry(NULL, NULL);
	tmp->size = 0;
	return tmp;
}

void delete_hash(HashTable* table) {
	for ( int i = 0 ; i < table->size ; i++ ) {
		HashTableEntry* cur_entry = table->table[i];
		HashTableEntry* old_entry = cur_entry;
		while ( cur_entry != NULL ) {
			old_entry = cur_entry;
			cur_entry = cur_entry->next;
			free(old_entry);
			table->table[i] = NULL;
		}
	}
	free(table->table);
	table->table = NULL;
	free(table);
	table = NULL;
}

HashTable* set_hash(HashTable* table, HashTableEntry* entry) {
	if ( table == NULL || entry == NULL )
		return table;
	uint32_t hash_value = generate_hash(entry->key);
	if ( hash_value >= table->size ) { // if hash is outside of table size
		table->table = realloc(table->table, sizeof(HashTableEntry*) * (hash_value + 1)); // resize table and initialize new space
		//printf("Initialize memory from %d to %d\n", table->size, hash_value);
		memset(&table->table[table->size], 0, (hash_value + 1 - table->size) * sizeof(HashTableEntry*));
		table->size = hash_value+1;
	}
	if ( table->table[hash_value] != NULL ) { // if there is already an entry at index
		HashTableEntry* cur_entry = table->table[hash_value];
		while ( cur_entry->next != NULL ) { // loop through linked list until you find the last entry
			cur_entry = cur_entry->next; // append new entry to the end
		}
		cur_entry->next = entry;
		entry->next = NULL;
	} else { // if there is no entry at index
		table->table[hash_value] = entry; // add new entry like normal
		entry->next = NULL;
	}
	table->last_entry = entry;
	return table;
}

HashTable* replace_hash(HashTable* table, char* dst, char* src) {
	HashTableEntry* dst_entry = get_hash(table, dst);
	uint32_t old_hash = generate_hash(dst);
	table->table[old_hash] = NULL;
	dst_entry->key = src;
	set_hash(table, dst_entry);
	return table;
}

HashTableEntry* get_hash(HashTable* table, char* key) {
	if ( table == NULL || key == NULL || table->table == NULL )
		return NULL;
	uint32_t hash_value = generate_hash(key);
	if ( hash_value < table->size && table->table[hash_value] != NULL ) { // if there is an entry at index
		HashTableEntry* cur_entry = table->table[hash_value]; // verify that it is the correct key
		while ( cur_entry != NULL ) { // if not, loop through the list until you find the correct key
			if ( strcmp(cur_entry->key, key) == 0 ) {
				return cur_entry;
			}
			cur_entry = cur_entry->next;
		}
	}
	return NULL; // return NULL if no entry with correct key is found
}

void* get_hash_value(HashTable* table, char* key) {
	HashTableEntry* entry = get_hash(table, key);
	if ( entry != NULL ) {
		return get_hash(table, key)->value;
	}
	return NULL;
}
